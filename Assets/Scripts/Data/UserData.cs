using System;
using System.Collections.Generic;

[Serializable]
public class UserData
{
    public string nameUser;
    public int coinUser;
    public int levelHome;
    public int valueEnegryRestore;
    public int timeEnegryRestore;
    public List<int> unlockedId;
    public List<int> valueEnegry;
    public List<int> valueJobLow;
    public List<int> valueJobHigh;
    public List<int> valueSteal;
   
    
}
