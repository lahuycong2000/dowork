using System;
using System.Collections.Generic;

[Serializable]
public class WheelData
{
    public ImageWheelData[] imageWheelDatas;
    public JobWheelData[] jobWheelDatas;
}

[Serializable]
public class ImageWheelData
{
    public string nameImage;
}

[Serializable]
public class JobWheelData
{
    public int lowOrHigh;
    public string textJob;
}