using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Ensign;
using UnityEngine.SceneManagement;

namespace DoWork
{
    public class SaveLoadJson : MonoBehaviour
    {
        public const string PATH_USER_OTHERS = "Data/UserDataOther";
        public const string PATH_WHEEL_DATA = "Main/WheelData";
        public const string PATH_IMAGE_DATA = "Home/ItemData";

        [Header("DATA_INPUT")]
        [SerializeField] private TMP_InputField _coin;
        [SerializeField] private TMP_InputField _level;

        [Header("RESTORE_ENERGY")]
        [SerializeField] private TMP_InputField _valueEnergyRestore;
        [SerializeField] private TMP_InputField _timeEnergyRestore;

        [Header("ENERGY")]
        [SerializeField] private TMP_InputField _minEnergy;
        [SerializeField] private TMP_InputField _maxEnergy;

        [Header("JOBLOW")]
        [SerializeField] private TMP_InputField _minJobLow;
        [SerializeField] private TMP_InputField _maxJobLow;

        [Header("JOBHIGH")]
        [SerializeField] private TMP_InputField _minJobHigh;
        [SerializeField] private TMP_InputField _maxJobHigh;

        [Header("VALUESTEAL")]
        [SerializeField] private TMP_InputField _minValueSteal;
        [SerializeField] private TMP_InputField _maxValueSteal;

        [Header("DATA_LIST")]
        [SerializeField] private List<int> _listJobLow;
        [SerializeField] private List<int> _listJobHigh;
        [SerializeField] private List<string> _iconWheel;
        [SerializeField] private List<string> _imageItem;
        [SerializeField] private List<int> _priceItem;

        private UserDataOther _userDataOther;
        private WheelData _wheelData;
        private ItemData _itemData;

        public UserDataOther userDataOther { get => _userDataOther; set => _userDataOther = value; }
        public WheelData WheelData { get => _wheelData; set => _wheelData = value; }
        public ItemData ItemData { get => _itemData; set => _itemData = value; }
        public List<int> ListJobLow { get => _listJobLow; set => _listJobLow = value; }
        public List<int> ListJobHigh { get => _listJobHigh; set => _listJobHigh = value; }
        public List<string> IconWheel { get => _iconWheel; set => _iconWheel = value; }
        public List<string> ImageItem { get => _imageItem; set => _imageItem = value; }
        public List<int> PriceItem { get => _priceItem; set => _priceItem = value; }

        public void AddNew()
        {       
            UserData data = new UserData();
            data.coinUser = int.Parse(_coin.text);
            data.levelHome = int.Parse(_level.text);
            data.valueEnegryRestore = int.Parse(_valueEnergyRestore.text);
            data.timeEnegryRestore = int.Parse(_timeEnergyRestore.text);
            data.unlockedId = new List<int>() { };
            data.valueEnegry = new List<int>() { int.Parse(_minEnergy.text), int.Parse(_maxEnergy.text) };
            data.valueJobLow = new List<int>() { int.Parse(_minJobLow.text), int.Parse(_maxJobLow.text) };
            data.valueJobHigh = new List<int>() { int.Parse(_minJobHigh.text), int.Parse(_maxJobHigh.text) };
            data.valueSteal = new List<int>() { int.Parse(_minValueSteal.text), int.Parse(_maxValueSteal.text) };
            if (data.coinUser == 0 && data.coinUser == 1)
            {
                data.coinUser = 1000;
            }
            if (data.levelHome != 1 || data.levelHome != 2)
            {
                data.levelHome = 1;
            }
            int sub = GameManager.Instance.ManagerEnergy.MaxEnergy - GameManager.Instance.ManagerEnergy.TotalEnergy;
            GameManager.Instance.ManagerEnergy.AddEnergy(sub);
            SaveToJson(data);
            SceneManager.LoadScene("DoWorkScene");
        }
        public void AddFirstPlay()
        {
            UserData data = new UserData();
            data.coinUser = 10000;
            data.levelHome = 1;
            data.valueEnegryRestore = 1;
            data.timeEnegryRestore = 600;
            data.unlockedId = new List<int>() { };
            data.valueEnegry = new List<int>() { 1, 3 };
            data.valueJobLow = new List<int>() { 100, 200 };
            data.valueJobHigh = new List<int>() { 300, 400 };
            data.valueSteal = new List<int>(){ 300, 700 };
            GameManager.Instance.ManagerEnergy.AddEnergy(50);
            SaveToJson(data);
        }
        public void SaveToJson(UserData data)
        {
            string json = data.ToJsonFormat();
            PlayerPrefs.SetString("LeaderBoard", json);
            GameManager.Instance.ManagerCoin.UpdateInfo();
        }
        public UserData LoadFromJson()
        {
            string json = PlayerPrefs.GetString("LeaderBoard", "");
            UserData data = string.IsNullOrEmpty(json) ? new UserData() : json.JsonToObject<UserData>();
            //Debug.Log(json);
            return data;
        }
        public int TakeCoin()
        {
            int number = LoadFromJson().coinUser;
            return number;
        }
        public string TakeName()
        {
            string text = LoadFromJson().nameUser;
            return text;
        }
        public int TakelevelHome()
        {
            int number = LoadFromJson().levelHome;
            return number;
        }
        public void SaveNewData(int numberCoin, int numberItem)
        {
            UserData data = LoadFromJson();
            data.coinUser = numberCoin;
            if (numberItem != 0)
            {
                data.unlockedId.Add(numberItem);
            }
            SaveToJson(data);
        }
        public void AddName(string name)
        {
            UserData data = LoadFromJson();
            data.nameUser = name;
            SaveToJson(data);
        }
        public void LevelUp(int number)
        {
            UserData data = LoadFromJson();
            if (data.levelHome == 2)
            {
                data.levelHome = 1;
            }
            else
            {
                data.levelHome += number;
            }
            SaveToJson(data);
        }
        public void ResetUserData()
        {
            UserData data = new UserData();
            SaveToJson(data);
        }
        public void ResetUnlockedId()
        {
            UserData data = LoadFromJson();
            data.unlockedId.Clear();
            SaveToJson(data);

            _imageItem.Clear();
            _priceItem.Clear();
            ReadDataImageItem();
        }

        public void ReadDataUsersOther()
        {
            var dataJson = Resources.Load<TextAsset>(PATH_USER_OTHERS);
            _userDataOther = dataJson.text.JsonToObject<UserDataOther>();
        }
        public void ReadWheelData()
        {
            var dataJson = Resources.Load<TextAsset>(PATH_WHEEL_DATA);
            _wheelData = dataJson.text.JsonToObject<WheelData>();
            for (int i = 0; i < WheelData.jobWheelDatas.Length; i++)
            {
                if (WheelData.jobWheelDatas[i].lowOrHigh == 0)
                {
                    _listJobLow.Add(i);
                }
                else
                {
                    _listJobHigh.Add(i);
                }
            }
            for (int j = 0; j < WheelData.imageWheelDatas.Length; j++)
            {
                _iconWheel.Add(WheelData.imageWheelDatas[j].nameImage);
            }
        }
        public void ReadDataImageItem()
        {
            var dataJson = Resources.Load<TextAsset>(PATH_IMAGE_DATA);
            _itemData = dataJson.text.JsonToObject<ItemData>();
            for (int i = 0; i < ItemData.houseLv[TakelevelHome() - 1].furnitures.Length; i++)
            {
                _imageItem.Add(ItemData.houseLv[TakelevelHome() - 1].furnitures[i].nameImage);
                _priceItem.Add(ItemData.houseLv[TakelevelHome() - 1].furnitures[i].price * 1000);
            }
        }
    }
}
