using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Ensign.Unity;
using Ensign.UniRx;

namespace DoWork
{
    public class ItemSteal : MonoBehaviour
    {
        [SerializeField] private ItemSteal _item;
        [SerializeField] private Button _buttonItem;
        [SerializeField] private Image _imageItem;
        [SerializeField] private TextMeshProUGUI _nameItem;
        [SerializeField] private TextMeshProUGUI _coinItem;
        [SerializeField] private GameObject _panel;
        private int _coin;
        private int _count;
        public TextMeshProUGUI CoinItem { get => _coinItem; set => _coinItem = value; }
        public GameObject Panel { get => _panel; set => _panel = value; }

        public IObservable<Unit> OnButtonClicked => _buttonItem.OnClickAsObservable();
        private void Start()
        {
            _item.transform.localScale = new Vector3(1, 1, 1);
            _coinItem.gameObject.SetActive(false);
            OnButtonClicked.Subscribe(_ => SelectHouseSteal(_coin));
            _count = 0;
        }

        public void Initialize(Sprite icon, string name, int prize)
        {
            _imageItem.sprite = icon;
            _nameItem.text = name;
            _coin = prize;
            _coinItem.text = String.Format("{0:#,0} đ", prize);
        }

        public void SelectHouseSteal(int number)
        {
            _count++;
            if (_count == 1)
            {
                GameManager.Instance.ManagerCoin.AddCoin(number);
                GameManager.Instance.ManagerSteal.SetActiveOthersUser();
                _panel.gameObject.SetActive(false);
                this.ActionWaitTime(2, () =>
                {
                    GameManager.Instance.ManagerUI.BackMain();
                });
            }
        }
    }
}
