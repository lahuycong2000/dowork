using System.Linq;
using System.Collections.Generic;
using Ensign.Unity;
using UnityEngine;
using UnityEngine.UI;

namespace DoWork
{
    public class ItemStealManager : MonoBehaviour
    {
        public const string PATH_ITEM = "Steal/413013-PDG012-394";
        public const string PATH_SUBITEM = "413013-PDG012-394_{0}";

        [SerializeField] private List<ItemSteal> _listItemSteal = new List<ItemSteal>();
        [SerializeField] private ItemSteal _prefabItem;
        [SerializeField] private RectTransform _content;
        [SerializeField] private int _count;

        public void InstantiateItem()
        {
            _content.anchoredPosition = new Vector2(0, 0);
            _listItemSteal.Clear();
            _content.DestroyAllChilren();
            for (int i = 0; i < _count; i++)
            {
                ItemSteal itemSell = Instantiate(_prefabItem, _content);
                _listItemSteal.Add(itemSell);
                Sprite sprite = LoadSubSprite(PATH_ITEM, string.Format(PATH_SUBITEM, GameManager.Instance.RandomBase(0, 8)));
                UserData data = GameManager.Instance.LoadSaveJson.LoadFromJson();
                itemSell.Initialize(sprite, SetDataUserOther(), GameManager.Instance.RandomBase(data.valueSteal[0] *1000, data.valueSteal[1] *1000));
            }
        }
        private Sprite LoadSubSprite(string pathType, string name)
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>(PATH_ITEM);
            return sprites.FirstOrDefault(s => string.Compare(s.name.ToUpper(), name.ToUpper()) == 0);
        }
        public void SetActiveOthersUser()
        {
            foreach (var item in _listItemSteal)
            {
                item.CoinItem.gameObject.SetActive(true);
                item.Panel.gameObject.SetActive(true);
            }
        }
        public string SetDataUserOther()
        {
            UserDataOther userOthers = GameManager.Instance.LoadSaveJson.userDataOther;
            int random = Random.Range(0, GameManager.Instance.LoadSaveJson.userDataOther.user.Length);
            string name = userOthers.user[random].nameUser;
            return name;
        }
    }
}
