using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Ensign.Unity;

namespace DoWork
{
    public class EnergyManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _txtEnergy;
        [SerializeField] private TextMeshProUGUI _txtTime;
        [SerializeField] private Slider _sliderEnergy;
        [SerializeField] private int _maxEnergy;
        [SerializeField] private int _totalEnergy;
        private DateTime _nextEnergyTime;
        private DateTime _lastAddTime;
        private int _restoreDuration;
        private bool _restoring = false;
        private int _number;
        private UserData _userData;
        public int TotalEnergy { get => _totalEnergy; set => _totalEnergy = value; }
        public int MaxEnergy { get => _maxEnergy; set => _maxEnergy = value; }
        public bool Restoring { get => _restoring; set => _restoring = value; }

        public void Start()
        {
            LoadDataEnergy();
            // Debug.Log("_totalEnergy (!) " + _totalEnergy);
            SetSetting();
            StartCoroutine(RestoreRoutine());
        }
        public void LoadDataEnergy()
        {
            _totalEnergy = PlayerPrefs.GetInt("Energy");
            _nextEnergyTime = StringToDate(PlayerPrefs.GetString("nextEnergyTime"));
            _lastAddTime = StringToDate(PlayerPrefs.GetString("lastAddTime"));
        }
        public void SetSetting()
        {
            _userData = GameManager.Instance.LoadSaveJson.LoadFromJson();
            _restoreDuration = _userData.timeEnegryRestore;
            _number = _userData.valueEnegryRestore;
            _sliderEnergy.value = _totalEnergy;
            _sliderEnergy.maxValue = _maxEnergy;
        }
        public void AddEnergy(int number)
        {
            _totalEnergy += number;
            _sliderEnergy.value += number;
            UpdateTime();
            UpdateEnergy();
            SaveDataEnergy();
        }
        public void UseEnergy()
        {
            if (_totalEnergy == 0)
            {
                return;
            }
            if (_totalEnergy <= _maxEnergy)
            {
                _sliderEnergy.value--;
            }
            _totalEnergy--;
            UpdateEnergy();
            SaveDataEnergy();
            if (!_restoring)
            {
                if (_totalEnergy + 1 == _maxEnergy)
                {
                    _nextEnergyTime = AddDuration(DateTime.Now, _restoreDuration);
                }
                StartCoroutine(RestoreRoutine());
            }
        }
        private IEnumerator RestoreRoutine()
        {
            UpdateTime();
            UpdateEnergy();
            _restoring = true;
            while (_totalEnergy < _maxEnergy)
            {
                DateTime currTime = DateTime.Now;
                DateTime counter = _nextEnergyTime;
                bool isAdding = false;
                while (currTime > counter)
                {
                    if (_totalEnergy < _maxEnergy)
                    {
                        isAdding = true;
                        _totalEnergy += _number;
                        if (_totalEnergy > _maxEnergy)
                        {
                            _totalEnergy = _maxEnergy;
                        }
                        //Debug.Log(_totalEnergy);
                        _sliderEnergy.value += _number;
                        DateTime timeToAdd = _lastAddTime > counter ? _lastAddTime : counter;
                        counter = AddDuration(timeToAdd, _restoreDuration);
                    }
                    else
                    {
                        break;
                    }
                }
                if (isAdding)
                {
                    _lastAddTime = DateTime.Now;
                    _nextEnergyTime = counter;
                }
                UpdateTime();
                UpdateEnergy();
                SaveDataEnergy();
                yield return null;
            }
            //Debug.Log(" RestorRoutine " + _totalEnergy);
            _restoring = false;
        }
        public void UpdateTime()
        {
            if (_totalEnergy >= _maxEnergy)
            {
                _txtTime.text = "";
                return;
            }
            TimeSpan time = _nextEnergyTime - DateTime.Now;
            string value = String.Format("{0}:{1:D2}:{2:D2}", (int)time.TotalHours, time.Minutes, time.Seconds);
            _txtTime.text = value;
        }
        public void UpdateEnergy()
        {
            _txtEnergy.text = _totalEnergy.ToString();
        }
        private DateTime AddDuration(DateTime time, int duration)
        {
            return time.AddSeconds(duration);
        }
        public void SaveDataEnergy()
        {
            PlayerPrefs.SetInt("Energy", _totalEnergy);
            PlayerPrefs.SetString("nextEnergyTime", _nextEnergyTime.ToString());
            PlayerPrefs.SetString("lastAddTime", _lastAddTime.ToString());
        }
        private DateTime StringToDate(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return DateTime.Now;
            }
            return DateTime.Parse(date);
        }
    }
}
