using System;
using System.Collections.Generic;
using UnityEngine;
using Ensign.Unity;
using TMPro;
using UnityEngine.UI;
using Ensign.UniRx;

namespace DoWork
{
    public class GameManager : Singleton<GameManager>
    {
        [Header("MANAGER")]
        [SerializeField] private CoinManager _coinManager;
        [SerializeField] private SaveLoadJson _saveLoadJson;
        [SerializeField] private FurnitureManager _furnitureManager;
        [SerializeField] private ItemStealManager _itemStealManager;
        [SerializeField] private UIManager _uiManager;
        [SerializeField] private SpinManager _spinManager;
        [SerializeField] private SfxManager _sfxManager;
        [SerializeField] private EnergyManager _energyManage;
        [SerializeField] private LoadingScene _loadingScene;


        [Header("OTHERS")]
        [SerializeField] private TMP_InputField _textName;
        [SerializeField] private Button _bntName;
        [SerializeField] private TextMeshProUGUI _nameUser;
        private int _countPlayGame;


        public IObservable<Unit> OnButtonClicked => _bntName.OnClickAsObservable();
        public CoinManager ManagerCoin { get => _coinManager; set => _coinManager = value; }
        public SaveLoadJson LoadSaveJson { get => _saveLoadJson; set => _saveLoadJson = value; }
        public FurnitureManager ManagerFurniture { get => _furnitureManager; set => _furnitureManager = value; }
        public ItemStealManager ManagerSteal { get => _itemStealManager; set => _itemStealManager = value; }
        public UIManager ManagerUI { get => _uiManager; set => _uiManager = value; }
        public SpinManager ManagerSpin { get => _spinManager; set => _spinManager = value; }
        public SfxManager ManagerSfx { get => _sfxManager; set => _sfxManager = value; }
        public EnergyManager ManagerEnergy { get => _energyManage; set => _energyManage = value; }
        public LoadingScene ManagerLoading { get => _loadingScene; set => _loadingScene = value; }

        private void Awake()
        {
            if (!PlayerPrefs.HasKey("LeaderBoard"))
            {
                _saveLoadJson.AddFirstPlay();
            }
            _saveLoadJson.ReadDataUsersOther();
            _saveLoadJson.ReadWheelData();
            _saveLoadJson.ReadDataImageItem();
        }
        public void Start()
        {
            _uiManager.SplashScene.gameObject.SetActive(true);
            _textName.characterLimit = 14;
        }
        public void SetUp()
        {
            _textName.text = _saveLoadJson.TakeName();
            _uiManager.PopupName.gameObject.SetActive(string.IsNullOrEmpty(LoadSaveJson.TakeName()));
            OnButtonClicked.Subscribe(_ => ControlerPopupName());
            _sfxManager.PlayEffect(GameManager.Instance.ManagerSfx.SfxBegin);
        }
        public int RandomBase(int min, int max)
        {
            int number = UnityEngine.Random.Range(min, max);
            return number;
        }
        public void ControlerPopupName()
        {
            LoadSaveJson.AddName(_textName.text);
            _uiManager.PopupName.gameObject.SetActive(false);
        }
    }
}
