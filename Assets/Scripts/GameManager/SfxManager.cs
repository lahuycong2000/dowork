using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxManager : MonoBehaviour
{
    [SerializeField] private AudioSource _sourceAudio;
    [SerializeField] private AudioClip _sfxCoin;
    [SerializeField] private AudioClip _sfxBegin;
    [SerializeField] private AudioClip _sfxNoMoney;

    public AudioClip SfxCoin { get => _sfxCoin; set => _sfxCoin = value; }
    public AudioClip SfxBegin { get => _sfxBegin; set => _sfxBegin = value; }
    public AudioClip SfxNoMoney { get => _sfxNoMoney; set => _sfxNoMoney = value; }
    
    
    public void PlayEffect(AudioClip audioClip)
    {
        _sourceAudio.clip = audioClip;
        _sourceAudio.Play();
    }
}
