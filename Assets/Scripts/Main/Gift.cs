using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Gift : MonoBehaviour
{
    [SerializeField] private Image _imageGift;
    [SerializeField] private TextMeshProUGUI _textGift;

    private int _data;
    public int Data => _data;

    public void Initialize(int data, string pathItem, string name)
    {
        _data = data;
        _imageGift.sprite = LoadSprite(pathItem, name);
    }
    private Sprite LoadSprite(string pathType, string name)
    {
        string path = string.Format(pathType, name);
        Sprite sprite = Resources.Load<Sprite>(path);
        return sprite;
    }
}
