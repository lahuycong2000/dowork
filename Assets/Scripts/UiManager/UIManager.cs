using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign.Unity;
namespace DoWork
{
    public class UIManager : Singleton<UIManager>
    {
        [Header("REST_TRANSFORM")]
        [SerializeField] private Transform _homeScene;
        [SerializeField] private Transform _mainScene;
        [SerializeField] private Transform _stealScene;
        [SerializeField] private Transform _testScene;
        [SerializeField] private Transform _splashScene;

        [Header("POPUP_SELECT_SCENE")]
        [SerializeField] private Button _btnPopupSelectScene;
        [SerializeField] private PopupSelect _popupSelectScene;
        [SerializeField] private Vector3 _popupSelectPos;

        [Header("POPUP_BUY_FURNITURE")]
        [SerializeField] private Button _btnPopupOpenItem;
        [SerializeField] private Button _btnPopupCloseItem;
        [SerializeField] private PopupSelect _popupBuyFurniture;
        [SerializeField] private Vector3 _popupPos;

        [Header("OTHERS")]
        [SerializeField] private GameObject _popupLevelUp;
        [SerializeField] private GameObject _popupName;
        [SerializeField] private Button _btnPlayGame;
        public GameObject PopupLevelUp { get => _popupLevelUp; set => _popupLevelUp = value; }
        public GameObject PopupName { get => _popupName; set => _popupName = value; }
        public Transform SplashScene { get => _splashScene; set => _splashScene = value; }

        private EMainUI _currentTab;
        public EMainUI CurrentTab
        {
            get => _currentTab;
            set
            {
                _currentTab = value;
                UpdateUI(_currentTab);
                switch (_currentTab)
                {
                    case EMainUI.MainScene:
                        break;
                    case EMainUI.HomeScene:
                        break;
                    case EMainUI.StealScene:
                        break;
                    case EMainUI.TestScene:
                        break;
                }
            }
        }
        private void OnEnable()
        {
            ChangeTab(EMainUI.MainScene);
            _btnPopupSelectScene.onClick.AddListener(PopupSelectIn);
            _btnPopupOpenItem.onClick.AddListener(OpenPopupBuy);
            _btnPopupCloseItem.onClick.AddListener(ClosePopupBuy);
            _btnPlayGame.onClick.AddListener(LoadingScene);
        }
        private void OnDisable()
        {

        }
        public void LoadingScene()
        {
            _splashScene.gameObject.SetActive(false);
            GameManager.Instance.ManagerLoading.LoadScene();
        }
        private void PopupSelectIn()
        {
            if (GameManager.Instance.ManagerSpin.IsRunWheel == false)
            {
                _popupSelectPos = _popupSelectScene.transform.position;
                TweenMove(_popupSelectScene, _mainScene.transform.position, 0.5f);
            }
        }
        private void PopupSelectOut()
        {
            if (_popupSelectPos != Vector3.zero)
            {
                TweenMove(_popupSelectScene, _popupSelectPos, 0.5f);
            }
        }
        public void ChangeTab(EMainUI data)
        {
            CurrentTab = data;
        }
        private void OpenPopupBuy()
        {
            _popupPos = _popupBuyFurniture.transform.position;
            Vector2 popup = new Vector2(_homeScene.transform.position.x, _stealScene.transform.position.y);
            TweenMove(_popupBuyFurniture, popup, 0.5f);
            CheckPopup(1);
        }
        public void CheckPopup(int number)
        {
            _popupSelectScene.gameObject.SetActive(number == 0);
            _btnPopupCloseItem.gameObject.SetActive(number == 1);
        }
        public void ClosePopupBuy()
        {
            if (_popupPos != Vector3.zero)
            {
                TweenMove(_popupBuyFurniture, _popupPos, 0.3f);
            }
            CheckPopup(0);
        }
        public void UpdateUI(EMainUI tab)
        {
            _homeScene.gameObject.SetActive(tab == EMainUI.HomeScene);
            _mainScene.gameObject.SetActive(tab == EMainUI.MainScene);
            _stealScene.gameObject.SetActive(tab == EMainUI.StealScene);
            _testScene.gameObject.SetActive(tab == EMainUI.TestScene);
            PopupSelectOut();
            if (tab == EMainUI.StealScene)
            {
                CheckScene();
            }
        }
        public void TweenMove(PopupSelect currentPopup, Vector3 cellSelected, float time)
        {
            currentPopup.gameObject.transform.TweenMove(cellSelected, time);
        }
        public void BackMain()
        {
            ChangeTab(EMainUI.MainScene);
        }
        public void CheckScene()
        {
            GameManager.Instance.ManagerSteal.InstantiateItem();
        }
    }
    public enum EMainUI
    {
        HomeScene,
        MainScene,
        StealScene,
        TestScene
    }
}
