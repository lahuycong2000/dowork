using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DoWork
{
    public class PopupSelect : MonoBehaviour
    {
        [SerializeField] private UIManager _uiManager;
        [SerializeField] private List<ButtonTab> _buttonTabs;
        private void OnEnable()
        {
            _buttonTabs.ForEach(btn => btn.OnClicked += _uiManager.ChangeTab);
        }
    }
}
