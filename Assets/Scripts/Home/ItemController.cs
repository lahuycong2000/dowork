using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    [SerializeField] private List<Furniture> _furniture;
    public List<Furniture> Furniture { get => _furniture; set => _furniture = value; }
    
}
