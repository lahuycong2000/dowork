using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Ensign.UniRx;
using Ensign.Unity;
namespace DoWork
{
    public class ItemHome : MonoBehaviour
    {
        [SerializeField] private ItemHome _item;
        [SerializeField] private Button _buttonItem;
        [SerializeField] private Image _imageItem;
        [SerializeField] private TextMeshProUGUI _nameItem;
        [SerializeField] private TextMeshProUGUI _coinItem;
        [SerializeField] private GameObject _panel;
        [SerializeField] private int _itemId;
        private int _price;
        private int _count;

        public IObservable<Unit> OnButtonClicked => _buttonItem.OnClickAsObservable();
        public GameObject Panel { get => _panel; set => _panel = value; }
        private void Start()
        {
            // _count = 0;
            _item.transform.localScale = new Vector3(1, 1, 1);
            OnButtonClicked.Subscribe(_ => BuyFurniture(_price, _itemId));
        }
        public void BuyFurniture(int numberCoin, int id)
        {
            GameManager.Instance.ManagerCoin.RemoveCoin(numberCoin, id + 1);
            if (GameManager.Instance.ManagerCoin.Index == 1)
            {
                _panel.gameObject.SetActive(true);
                GameManager.Instance.ManagerUI.ClosePopupBuy();
                GameManager.Instance.ManagerFurniture.SetActiveItem(id);
            }
            // }
        }
        public void Initialize(int Id, string name, int price, string pathItem, string nameImage)
        {
            _itemId = Id;
            _imageItem.sprite = LoadSprite(pathItem, nameImage);
            _nameItem.text = name;
            _price = price;
            _coinItem.text = String.Format("{0:#,0} đ", price);
        }
        private Sprite LoadSprite(string pathType, string nameImage)
        {
            string path = string.Format(pathType, nameImage);
            Sprite sprite = Resources.Load<Sprite>(path);
            return sprite;
        }
    }
}
