using System.Collections;
using System.Collections.Generic;
using Ensign.Unity;
using UnityEngine;
using UnityEngine.UI;

namespace DoWork
{
    public class FurnitureManager : MonoBehaviour
    {
        public const string PATH_HOUSE = "Home/House_{0}";
        public const string PATH_IMAGE = "Home/Image/{0}";
        [SerializeField] private List<ItemHome> _listItemHome = new List<ItemHome>();
        [SerializeField] private ItemController _prefabFurni;
        [SerializeField] private ItemHome _prefabItem;
        [SerializeField] private RectTransform _content;
        [SerializeField] private GameObject _object3D;
        private int _countFur;
        private int _currnetFur;

        void Start()
        {
            InstantiateItem();
            SetScroll();
        }
        public void SetScroll()
        {
            GridLayoutGroup grid = _content.GetComponent<GridLayoutGroup>();
            _content.SetHeight(_listItemHome.Count * grid.cellSize.y + grid.padding.bottom * 2 + grid.spacing.y * _listItemHome.Count);
        }

        public void SetActiveItem(int number)
        {
            this.ActionWaitTime(1, () =>
                 {
                     _prefabFurni.Furniture[number].gameObject.SetActive(true);
                     //Debug.Log("Number "+ number);
                     UserData data = GameManager.Instance.LoadSaveJson.LoadFromJson();

                     //Debug.Log(data.unlockedId);
                     _currnetFur = data.unlockedId.Count;
                     this.ActionWaitTime(1, () =>
                    {
                        CheckFullFurniture();
                    });
                 });

        }
        public void InstantiateItem()
        {
            _listItemHome.Clear();
            _content.DestroyAllChilren();
            _object3D.DestroyAllChilren();
            UserData data = GameManager.Instance.LoadSaveJson.LoadFromJson();
            _prefabFurni = InstantiateObject(PATH_HOUSE, data.levelHome.ToString());
            _countFur = _prefabFurni.Furniture.Count;
            InstantiateItemUI();
            SetDataId();
            if (data.unlockedId.Count != 0)
            {
                for (int i = 0; i < data.unlockedId.Count; i++)
                {
                    _prefabFurni.Furniture[data.unlockedId[i] - 1].gameObject.SetActive(true);
                    _listItemHome[data.unlockedId[i] - 1].Panel.gameObject.SetActive(true);
                }
            }
        }
        public void InstantiateItemUI()
        {
            for (int i = 0; i < _countFur; i++)
            {
                ItemHome itemSell = Instantiate(_prefabItem, _content.transform);
                _listItemHome.Add(itemSell);
            }
        }
        public void SetDataId()
        {
            for (int i = 0; i < _prefabFurni.Furniture.Count; i++)
            {
                _prefabFurni.Furniture[i].gameObject.SetActive(false);
                SaveLoadJson item = GameManager.Instance.LoadSaveJson;
                _listItemHome[i].Initialize(i, item.ImageItem[i], item.PriceItem[i], PATH_IMAGE, item.ImageItem[i]);
            }
        }
        private ItemController InstantiateObject(string path, string nameImage)
        {
            string pathNew = string.Format(path, nameImage);
            ItemController item = Instantiate(Resources.Load<ItemController>(pathNew), _object3D.transform);
            return item;
        }
        public void CheckFullFurniture()
        {
            if (_currnetFur == _countFur)
            {
                GameManager.Instance.ManagerUI.PopupLevelUp.gameObject.SetActive(true);
                this.ActionWaitTime(7, () =>
                {
                    GameManager.Instance.ManagerUI.PopupLevelUp.gameObject.SetActive(false);
                    GameManager.Instance.LoadSaveJson.LevelUp(1);
                    GameManager.Instance.LoadSaveJson.ResetUnlockedId();
                    InstantiateItem();
                });
            }
        }
    }
}
